package com.example.louvana

import android.media.AudioFormat
import android.media.MediaPlayer
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.Button
import com.example.louvana.databinding.ActivityMainBinding
import android.media.AudioTrack

import android.media.AudioManager




class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var gButton = findViewById<Button>(R.id.gButton)
        gButton.setOnClickListener{
            generateTone(392.0, 3000)?.play()
        }

        var cButton = findViewById<Button>(R.id.cButton)
        cButton.setOnClickListener{
            generateTone(261.63, 3000)?.play() }

        var eButton = findViewById<Button>(R.id.eButton)
        eButton.setOnClickListener{
            generateTone(329.63, 3000)?.play()
        }

        var aButton = findViewById<Button>(R.id.aButton)
        aButton.setOnClickListener{
            generateTone(440.0, 3000)?.play()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun playG(view: View) {
        System.out.println("G!")
    }

    private fun generateTone(freqHz: Double, durationMs: Int): AudioTrack? {
        val count = (44100.0 * 2.0 * (durationMs / 1000.0)).toInt() and 1.inv()
        val samples = ShortArray(count)
        var i = 0
        while (i < count) {
            val sample = (Math.sin(2 * Math.PI * i / (44100.0 / freqHz)) * 0x7FFF).toInt().toShort()
            samples[i + 0] = sample
            samples[i + 1] = sample
            i += 2
        }
        val track = AudioTrack(
            AudioManager.STREAM_MUSIC, 44100,
            AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT,
            count * (java.lang.Short.SIZE / 8), AudioTrack.MODE_STATIC
        )
        track.write(samples, 0, count)
        return track
    }

}